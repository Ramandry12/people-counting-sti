import { BrowserRouter as Router, Routes, Route, useNavigate} from "react-router-dom";
import Layout from "./app/components/layout/Layout";
import NavigateRoutes from "./NavigateRoutes";
import NotFoundPage from "./app/pages/404/NotFoundPage";
import ListDataDemo from "./app/pages/demo/ListDataDemo";
import StyleFeed from "./app/pages/demo/StyleFeed";
import StyleForm from "./app/pages/demo/StyleForm";
import UploadFormDemo from "./app/pages/demo/UploadFormDemo";
import ImageUploader from "./app/pages/demo/uploader/ImageUploader";
import TestUpload from "./app/pages/demo/TestUpload";
import ImageReader from "./app/pages/demo/uploader/ImageReader";
import Poi from "./app/pages/demo/Poi";
import SummaryFlowAnalyst from "./app/pages/summary-flow-analyst/SummaryFlowAnalyst";
import TableSummaryA from "./app/pages/demo/TableSummaryA";

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={(
          <NavigateRoutes>
            <Layout />
          </NavigateRoutes>
          )}>
            <Route path="*" element={<NotFoundPage />} />
            <Route path="/poi" element={<Poi />}/>
            <Route path="/poi-upload" element={<UploadFormDemo />}/>
            <Route path="/feed" element={<StyleFeed />}/>
            <Route path="/form" element={<StyleForm />}/>
            <Route path="/upload" element={<ImageUploader />}/>
            <Route path="/test-upload" element={<TestUpload />}/>
            <Route path="/reader" element={<ImageReader />}/>
            <Route path="/summary-flow-analyst" element={<SummaryFlowAnalyst />}/>
            <Route path="/table-summary-a" element={<TableSummaryA />}/>
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
