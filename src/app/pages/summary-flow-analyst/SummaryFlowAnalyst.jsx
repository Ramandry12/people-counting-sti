import React from 'react'
import Donut from '../../components/chart/Donut';
import VerticalBar from '../../components/chart/VerticalBar';
import { Tab } from '@headlessui/react';
import Line from '../../components/chart/Line';

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }

export default function SummaryFlowAnalyst() {

    return (
        <div>
            <Tab.Group>
                <Tab.List className='flex mb-6 mt-2'>
                    <Tab 
                    className={({ selected }) =>
                    classNames(
                      'py-2 px-4 rounded-l-lg text-sm font-medium leading-5 shadow',
                      selected
                        ? 'bg-blue-400 text-white shadow'
                        : 'hover:bg-white/[0.12]'
                    )
                  }>Vehicle</Tab>
                    <Tab
                     className={({ selected }) =>
                     classNames(
                       'py-2 px-4 rounded-r-lg text-sm font-medium leading-5 shadow',
                       selected
                         ? 'bg-blue-400 text-white shadow'
                         : 'hover:bg-white/[0.12]'
                     )
                   }>Passenger</Tab>
                </Tab.List>
                <Tab.Panels>
                    <Tab.Panel>
                    <div className='grid grid-cols-1 md:grid-cols-2 gap-4'>
                        <div className='border border-gray-300 rounded-md shadow-md py-4'>
                            <p className='py-2 text-center text-md font-bold'>Passengers by Vehicle</p>
                            <Donut />
                        </div>
                        <div className='border border-gray-300 rounded-md shadow-md py-4'>
                            <p className='py-2 text-center text-md font-bold'>Top 3 Vehicle</p>
                            <VerticalBar />
                        </div>
                    </div>
                    </Tab.Panel>
                    <Tab.Panel>
                        <div className='border border-gray-300 rounded-md shadow-md py-4'>
                            <p className='py-2 text-center text-md font-bold'>Passenger Statistics</p>
                            <Line />
                        </div>
                    </Tab.Panel>
                </Tab.Panels>
            </Tab.Group>
        </div>
    );
}
