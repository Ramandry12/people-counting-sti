import React, { useMemo } from 'react';
import { useTable, useFilters, useSortBy, usePagination } from 'react-table';
import { ChevronDoubleLeftIcon, ChevronLeftIcon, ChevronRightIcon, ChevronDoubleRightIcon } from '@heroicons/react/solid';
import { SortAscendingIcon, SortDescendingIcon } from '@heroicons/react/outline';
import data from './data';

const TableSummaryA = () => {
  const columns = useMemo(
    () => [
    //   {
    //     Header: 'No',
    //     accessor: (row, index) => index + 1,
    //   },
      {
        Header: 'Bus Name',
        accessor: 'busName',
      },
      {
        Header: 'Station Name',
        accessor: 'stationName',
      },
      {
        Header: 'Total Passengers',
        accessor: 'totalPassengers',
      },
      {
        Header: 'Route',
        accessor: 'route',
      },
      {
        Header: 'Time',
        accessor: 'time',
        Cell: ({ value }) => new Date(value).toLocaleString(),
      },
    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    state: { pageIndex, pageSize, filters, sortBy },
    gotoPage,
    previousPage,
    nextPage,
    canPreviousPage,
    canNextPage,
    pageCount,
    setPageSize,
    visibleColumns,
    rows,
    setFilter,
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 5, sortBy: [{ id: 'time', desc: true }] },
    },
    useFilters,
    useSortBy,
    usePagination
  );

  const totalRowCount = rows.length;
  const isDataEmpty = totalRowCount === 0;
  const isFilteredEmpty = isDataEmpty && filters && filters.length > 0;

  return (
    <div className="mx-auto max-w-7xl p-4">
        <div>
        <h2 className='text-xl font-bold pb-4'>Bus Summary</h2>
        </div>
      <div className="flex flex-1 md:flex-2 md:justify-between items-center mb-4">
        <div>
          <input
            type="text"
            value={filters ? filters.find(f => f.id === 'busName')?.value || '' : ''}
            onChange={(e) => setFilter('busName', e.target.value)}
            placeholder="Search by Bus Name"
            className="border p-2 md:mr-2 mb-2"
          />
          <input
            type="date"
            value={filters ? filters.find(f => f.id === 'time')?.value || '' : ''}
            onChange={(e) => setFilter('time', e.target.value)}
            placeholder="Filter by Date"
            className="border p-2"
          />
        </div>
      </div>
      <div className='overflow-x-auto'>
        <table {...getTableProps()} className="w-full border-collapse shadow-sm">
            <thead>
            {headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()} className="bg-gray-200">
                {headerGroup.headers.map((column) => (
                    <th
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                    className="py-2 px-4 border cursor-pointer"
                    >
                    <div className="flex items-center">
                        {column.render('Header')}
                        {column.canSort ? (
                        <button
                            type="button"
                            onClick={() => column.toggleSortBy(!column.isSortedDesc)}
                            className="focus:outline-none ml-2"
                        >
                            {column.isSorted ? (
                            column.isSortedDesc ? (
                                <SortDescendingIcon className="w-4 h-4 text-blue-500 ml-2" />
                            ) : (
                                <SortAscendingIcon className="w-4 h-4 text-blue-500 ml-2" />
                            )
                            ) : (
                            <React.Fragment>
                                <SortAscendingIcon className="w-2.5 h-2.5 text-gray-400 ml-2" />
                                <SortDescendingIcon className="w-2.5 h-2.5 text-gray-400 ml-2" />
                            </React.Fragment>
                            )}
                        </button>
                        ) : null}
                    </div>
                    </th>
                ))}
                </tr>
            ))}
            </thead>
            <tbody {...getTableBodyProps()}>
            {isDataEmpty ? (
            <tr>
                <td className="py-4 px-6 text-center text-gray-500 border" colSpan={columns.length}>
                No data available.
                </td>
            </tr>
            ) : (
            page.map((row, index) => {
                prepareRow(row);
                return (
                <tr {...row.getRowProps()} className="bg-white">
                    {row.cells.map((cell) => (
                    <td {...cell.getCellProps()} className="py-2 px-4 border">
                        {cell.render('Cell')}
                    </td>
                    ))}
                </tr>
                );
            })
            )}
        </tbody>
        </table>
      </div>
      {!isDataEmpty && isFilteredEmpty && (
        <div className="text-center text-gray-500">No matching data found.</div>
      )}
      <div className="md:flex md:justify-between items-center mt-4">
        <div className="flex items-center">
          <button onClick={() => gotoPage(0)} disabled={pageIndex === 0 || isDataEmpty} className={`px-4 py-1 rounded mr-2 ${pageIndex === 0 || isDataEmpty ? 'bg-gray-200 text-gray-300 cursor-not-allowed' : 'bg-blue-400 text-white'}`}>
            <ChevronDoubleLeftIcon className="w-5 h-5" />
          </button>
          <button onClick={() => previousPage()} disabled={!canPreviousPage || isDataEmpty} className={`px-4 py-1 rounded mr-2 ${!canPreviousPage || isDataEmpty ? 'bg-gray-200 text-gray-300 cursor-not-allowed' : 'bg-blue-400 text-white'}`}>
            <ChevronLeftIcon className="w-5 h-5" />
          </button>
          <button onClick={() => nextPage()} disabled={!canNextPage || isDataEmpty || isFilteredEmpty} className={`px-4 py-1 rounded mr-2 ${(!canNextPage || isDataEmpty || isFilteredEmpty) ? 'bg-gray-200 text-gray-300 cursor-not-allowed' : 'bg-blue-400 text-white'}`}>
            <ChevronRightIcon className="w-5 h-5" />
          </button>
          <button onClick={() => gotoPage(pageCount - 1)} disabled={pageIndex === pageCount - 1 || isDataEmpty} className={`px-4 py-1 rounded mr-2 ${pageIndex === pageCount - 1 || isDataEmpty ? 'bg-gray-200 text-gray-300 cursor-not-allowed' : 'bg-blue-400 text-white'}`}>
            <ChevronDoubleRightIcon className="w-5 h-5" />
          </button>
        </div>
        <div className='md:mt-0 mt-4'>
          <span>Show:</span>
          <select value={pageSize} onChange={(e) => setPageSize(Number(e.target.value))} className="border p-2 ml-2">
            {[5, 10, 15].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                {pageSize}
              </option>
            ))}
          </select>
          <span className="ml-4">
            Showing {page.length} of {isFilteredEmpty ? 0 : totalRowCount} rows
          </span>
        </div>
      </div>
    </div>
  );
};

export default TableSummaryA;
