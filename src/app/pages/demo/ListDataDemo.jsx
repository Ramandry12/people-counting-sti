/* eslint-disable jsx-a11y/no-redundant-roles */
import React from 'react'
// import { Data } from './Data'
import { MailIcon, PhoneIcon } from '@heroicons/react/outline'
import { HomeIcon } from '@heroicons/react/solid'

const people = [
    {
      name: 'Jane Cooper',
      title: 'Paradigm Representative',
      role: 'Admin',
      email: 'janecooper@example.com',
      telephone: '+1-202-555-0170',
      imageUrl:
        'https://images.unsplash.com/photo-1595974379321-45126887ab11?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
    },
    {
      name: 'Jane Cooper',
      title: 'Paradigm Representative',
      role: 'Admin',
      email: 'janecooper@example.com',
      telephone: '+1-202-555-0170',
      imageUrl:
        'https://images.unsplash.com/photo-1595974379321-45126887ab11?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
    },
    {
      name: 'Jane Cooper',
      title: 'Paradigm Representative',
      role: 'Admin',
      email: 'janecooper@example.com',
      telephone: '+1-202-555-0170',
      imageUrl:
        'https://images.unsplash.com/photo-1595974379321-45126887ab11?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
    },
    // More people...
  ]

  const datas = [
    {
      name: 'Jane Cooper',
      title: 'Paradigm Representative',
      role: 'Admin',
      email: 'janecooper@example.com',
      telephone: '+1-202-555-0170',
      imageUrl:
        'https://images.unsplash.com/photo-1595974379321-45126887ab11?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
    },
    // More people...
  ]

const ListDataDemo = () => {
    
  return (
    <div>
        {/* <div>header</div> */}
        <div className="bg-white">
            <div className="flex flex-1 gap-4">
                <select name="" id="" disabled className="text-sm w-1/6 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50">
                    <option value="">Custom</option>
                </select>
                <input disabled type="datetime-local" id="tanggal-waktu" name="tanggal-waktu" className="text-sm w-1/5 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"/>
                <input disabled type="datetime-local" id="tanggal-waktu" name="tanggal-waktu" className="text-sm w-1/5 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"/>
                <select disabled name="" id="" className="text-sm w-1/5 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50">
                    <option value="">BIH001</option>
                </select>
            </div>
            <div className="my-6 flex justify-between items-center">
                <select disabled name="" id="" className="text-sm w-1/6 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50">
                    <option value="">All</option>
                </select>
                <button
                    disabled
                    type="submit"
                    className="ml-3 inline-flex justify-center py-2 px-6 border border-transparent shadow-sm text-sm font-medium rounded-md bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"
                    >
                    Search
                </button>
                {/* <button
                    disabled
                    type="submit"
                    className="ml-3 inline-flex justify-center py-2 px-6 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-500 hover:bg-blue-600"
                    >
                    Search
                </button> */}
            </div>
        </div>
        <div className="bg-gray-100 p-2 pb-4 rounded-sm">
            <div className="grid grid-cols-1 md:grid-cols-4 gap-2">
                <div className="col-span-1">
                    <ul role="list" className="grid grid-cols-1">
                        {datas.map((person) => (
                        <li
                        key={person.email}
                        className="col-span-1 flex flex-col bg-white rounded-lg shadow divide-y divide-gray-200"
                            >
                            <div className="flex-1 flex flex-col p-2">
                                <div className="flex items-center">
                                    <HomeIcon
                                        className='text-black mr-1 flex-shrink-0 h-6 w-6'
                                        aria-hidden="true"
                                    />
                                    <span className="text-gray-900 text-sm font-medium">{person.name}</span>
                                    <span className="ml-2 text-blue-500 capitalize py-1 px-2 text-sm border border-blue-200 font-normal bg-blue-50 rounded-sm">
                                    {person.role}
                                    </span>
                                </div>
                                <div className="mt-4 text-sm">
                                    <div className="grid grid-cols-3 gap-5">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Bus Name
                                        </div>
                                        <div className="col-span-2">
                                            {person?.role}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Time
                                        </div>
                                        <div className="col-span-2">
                                            {person?.telephone}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Station Name
                                        </div>
                                        <div className="col-span-2">
                                            {person?.title}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Route
                                        </div>
                                        <div className="col-span-2">
                                            {person?.name}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Total
                                        </div>
                                        <div className="col-span-2">
                                            {person?.telephone}
                                        </div>
                                    </div>
                                </div>
                                <div className="my-4 text-center">
                                    <button className="px-6 py-2 text-white text-sm font-normal bg-blue-400 rounded-full">
                                    Confirm
                                    </button>
                                </div>
                            </div>
                        </li>
                        ))}
                    </ul>
                </div>
                <div className="col-span-3">
                    <ul role="list" className="grid grid-cols-1 gap-2 sm:grid-cols-2 md:grid-cols-3">
                    {people.map((person) => (
                        <li
                        key={person.email}
                        className="col-span-1 flex flex-col text-center bg-white rounded-lg shadow divide-y divide-gray-200"
                        >
                        <div className="flex-1 flex flex-col p-1">
                            <img className="w-auto h-52 flex-shrink-0 mx-auto rounded-sm" src={person.imageUrl} alt="" />
                        </div>
                        <div>
                            <div className="-mt-px flex">
                                <div className="w-0 flex-1 flex p-2 pr-6">
                                    <span className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center text-blue-500 capitalize py-1 px-2 text-xs border border-blue-200 font-normal bg-blue-50 rounded-sm">
                                        {person.telephone}
                                    </span>
                                </div>
                                <div className="w-0 flex-1 flex text-xs p-2">
                                    <div className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center">
                                        <div className="flex rounded-md shadow-sm">
                                            <div className="relative flex items-stretch flex-grow focus-within:z-10">
                                            <input
                                                type="text"
                                                name="email"
                                                id="email"
                                                className="w-full rounded-none rounded-l-sm  border-gray-300 border pl-2"
                                                placeholder="0"
                                            />
                                            </div>
                                            <button
                                            type="button"
                                            className="-ml-px relative inline-flex items-center space-x-2 px-4 py-1 border border-gray-300 rounded-r-sm text-gray-700 bg-gray-50 hover:bg-gray-100"
                                            >
                                                Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </li>
                    ))}
                    </ul>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ListDataDemo