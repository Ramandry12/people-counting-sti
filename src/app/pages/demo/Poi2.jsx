/* eslint-disable no-lone-blocks */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/no-redundant-roles */
import React, { useState } from 'react'
import { PhotographIcon, RefreshIcon } from '@heroicons/react/outline'
import { HomeIcon } from '@heroicons/react/solid'
import axios from 'axios'
import Spinner from '../../components/helper/Spinner'

const Poi2 = () => {

    const [img, setImg] = useState('')
    const [processImg, setProcessImg] = useState('')
    const [totalObj, setTotalObj] = useState(0)
    const [isLoading, setIsLoading] = useState(false);
    const [isProcessing, setIsProcessing] = useState(false);
  
    const handleChange = (e) => {
      console.log(e.target.files)
      const data = new FileReader();
      data.addEventListener('load', () => {
        setImg(data.result);
      })
      data.readAsDataURL(e.target.files[0]);
    }

    const uploadClick = (e) =>{
        e.preventDefault();
        const b64img = img.split(',')[1];
        console.log(img.split(',')[0])
        console.log({b64img})
    
        const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json'},
          body: JSON.stringify({ b64img })
        };
    
        const url = 'http://103.140.90.10:9091/people_counting';
        fetch(url, requestOptions)
          .then(response => response.json())
          .then(data => {
            // console.log({data})
            const objDetImg = data.b64img;
            setIsLoading(true)
            setIsProcessing(true)
            setTimeout(() => {
                setProcessImg(`data:image/jpeg;base64,${objDetImg}`)
                setTotalObj(data.object_detected)
                setIsProcessing(false);
                setIsLoading(false);
              }, 2000);
          })
          .catch((error) => {
            console.log(error.message)
          })
      }

      const datas = [
        {
          busName: 'BIH001',
          id: 1,
          time: '2023-04-10 07:25:57',
          station: 'POI2-TERMINAL P.GEBANG',
          route: 'Jakarta & Surabaya',
          total: totalObj,
        },
      ]

      const handleRefresh = () => {
        setImg('');
        setProcessImg('');
        setTotalObj(0);
      }
    
  return (
    <div>
        {/* <div>header</div> */}
        {/* <div className="bg-white">
            <div className="flex flex-1 gap-4">
                <select name="" id="" disabled className="text-sm w-1/6 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50">
                    <option value="">Custom</option>
                </select>
                <input disabled type="datetime-local" id="tanggal-waktu" name="tanggal-waktu" className="text-sm w-1/5 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"/>
                <input disabled type="datetime-local" id="tanggal-waktu" name="tanggal-waktu" className="text-sm w-1/5 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"/>
                <select disabled name="" id="" className="text-sm w-1/5 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50">
                    <option value="">BIH001</option>
                </select>
            </div>
            <div className="my-6 flex justify-between items-center">
                <select disabled name="" id="" className="text-sm w-1/6 py-2 px-2 rounded-md border border-gray-300 bg-gray-300 text-gray-500 cursor-not-allowed opacity-50">
                    <option value="">All</option>
                </select>
                <button
                    disabled
                    type="submit"
                    className="ml-3 inline-flex justify-center py-2 px-6 border border-transparent shadow-sm text-sm font-medium rounded-md bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"
                    >
                    Search
                </button>
            </div>
        </div> */}
        <div className="bg-gray-100 p-2 pb-4 rounded-sm">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-2">
                <div className="col-span-1">
                    <ul role="list" className="grid grid-cols-1">
                        {datas.map((data) => (
                        <li
                        key={data?.id}
                        className="col-span-1 flex flex-col bg-white rounded-lg shadow divide-y divide-gray-200"
                            >
                            <div className="flex-1 flex flex-col p-2">
                                <div className="flex items-center">
                                    <HomeIcon
                                        className='text-black mr-1 flex-shrink-0 h-6 w-6'
                                        aria-hidden="true"
                                    />
                                    <span className="text-gray-900 text-sm font-medium">Arrival</span>
                                    <span className="ml-2 text-blue-500 capitalize py-1 px-2 text-sm border border-blue-200 font-normal bg-blue-50 rounded-sm">
                                    Undone
                                    </span>
                                </div>
                                <div className="mt-4 text-sm">
                                    <div className="grid grid-cols-3 gap-5">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Bus Name
                                        </div>
                                        <div className="col-span-2">
                                            {data?.busName}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Time
                                        </div>
                                        <div className="col-span-2">
                                            {data?.time}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Station Name
                                        </div>
                                        <div className="col-span-2">
                                            {data?.station}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Route
                                        </div>
                                        <div className="col-span-2 uppercase">
                                            {data?.route}
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-3 gap-5 mt-2">
                                        <div className="col-span-1 text-gray-500 text-right">
                                            Total
                                        </div>
                                        <div className="col-span-2">
                                            {data?.total}
                                        </div>
                                    </div>
                                </div>
                                <div className="my-4 text-center">
                                    <button className="px-6 py-2 text-white text-sm font-normal bg-blue-400 rounded-full">
                                    Confirm
                                    </button>
                                </div>
                            </div>
                        </li>
                        ))}
                    </ul>
                </div>
                <div className="col-span-2">
                    <ul role="list" className="grid grid-cols-1 gap-2 sm:grid-cols-2">
                        <li
                        className="col-span-1 flex flex-col text-center bg-white rounded-lg shadow divide-y divide-gray-200"
                        >
                        <div className="flex-1 flex flex-col p-1">
                        {
                            img !== '' ?
                            <img className="w-auto h-52 flex-shrink-0 mx-auto rounded-sm" 
                            // src={people[0].imageUrl} 
                            src={img}
                            alt="Image" />
                            :
                            <div className="mt-1 h-52 flex justify-center px-6 py-12 border-2 border-gray-300 border-dashed rounded-md">
                            <div className="space-y-1 text-center">
                                <label htmlFor="img-upload">
                                <svg
                                className="cursor-pointer mx-auto h-24 w-24 text-gray-400"
                                stroke="currentColor"
                                fill="none"
                                viewBox="0 0 48 48"
                                aria-hidden="true"
                                >
                                <path
                                    d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                    strokeWidth={2}
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                </svg>
                                <input id="img-upload" name="img-upload" type="file" className="sr-only" onChange={handleChange}/>
                                </label>
                                {/* <div className="flex text-sm text-gray-600">
                                <label
                                    htmlFor="file-upload"
                                    className="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                                >
                                    <span>Upload a file</span>
                                    <input id="file-upload" name="file-upload" type="file" className="sr-only" onChange={handleChange}/>
                                </label>
                                <p className="pl-1">or drag and drop</p>
                                </div> */}
                                {/* <p className="text-xs text-gray-500">PNG, JPG, GIF up to 10MB</p> */}
                            </div>
                            </div>
                        }
                        </div>
                        <div>
                            <div className="-mt-px flex">
                                <div className="w-0 flex-1 flex p-2">
                                    {/* <span className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center text-blue-500 capitalize py-1 px-2 text-xs border border-blue-200 font-normal bg-blue-50 rounded-sm">
                                        123
                                    </span> */}
                                    {/* <input type='file' onChange={handleChange}/> */}
                                    <label
                                        htmlFor="file-upload"
                                        className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center cursor-pointer bg-blue-400 text-sm rounded-md py-1 px-2 font-normal text-white hover:bg-blue-500"
                                    >
                                        <span>Choose File</span>
                                        <input id="file-upload" name="file-upload" type="file" className="sr-only" onChange={handleChange}/>
                                    </label>
                                </div>
                                <div className="w-0 flex-1 flex text-xs p-2">
                                    <div className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center">
                                        <div className="flex rounded-md shadow-sm">
                                            {
                                                img === '' ?
                                                <button
                                                type="button"
                                                disabled={img === '' || isProcessing}
                                                onClick={uploadClick}
                                                className="-ml-px relative inline-flex items-center space-x-2 px-4 py-1 border border-gray-300 rounded-r-sm bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"
                                                >
                                                    Process
                                                </button>
                                                :
                                                <button
                                                type="button"
                                                disabled={img === '' || isProcessing}
                                                onClick={uploadClick}
                                                className={isProcessing ?
                                                    "-ml-px relative inline-flex items-center space-x-2 px-4 py-1 border border-gray-300 rounded-r-sm bg-gray-300 text-gray-500 cursor-not-allowed opacity-50"
                                                    :
                                                    "-ml-px relative inline-flex items-center space-x-2 px-4 py-1 border border-gray-300 rounded-r-sm text-gray-700 bg-gray-50 hover:bg-gray-100"}
                                                >
                                                    {
                                                    isProcessing ?
                                                    'Processing'
                                                    : 'Process'
                                                    }
                                                </button>
                                            }
                                        </div>
                                        <div className="px-4 flex justify-center items-center">
                                        <button disabled={img === ''} onClick={() => handleRefresh()}>
                                            <RefreshIcon className={img === '' ?
                                            "h-8 w-8 text-gray-300 cursor-not-allowed" :
                                            "h-8 w-8 text-gray-500"
                                            } />
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </li>
                        <li
                        className="col-span-1 flex flex-col text-center bg-white rounded-lg shadow divide-y divide-gray-200"
                        >
                        <div className="flex-1 flex flex-col p-1">
                        {
                            processImg !== '' ?
                            <img className="w-auto h-52 flex-shrink-0 mx-auto rounded-sm" 
                            // src={people[0].imageUrl} 
                            src={processImg} 
                            alt="processImg" />
                            :
                            <div className="mt-1 h-52 flex justify-center items-center px-6 py-12 border-2 border-gray-300 border-dashed rounded-md">
                            <div className="space-y-1 text-center">
                            {
                                isLoading ?
                                <div className="flex justify-center items-center">
                                <Spinner />
                                <span className="text-center">Processing...</span>
                                </div> 
                                :
                                <PhotographIcon className="h-24 w-auto text-gray-400 "/>
                            }
                            </div>
                            </div>
                        }
                        </div>
                        <div>
                            <div className="-mt-px flex col-span-3">
                                <div className="w-0 flex-1 flex p-2">
                                    <span className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center text-blue-500 capitalize py-1 px-2 text-sm border border-blue-200 font-normal bg-blue-50 rounded-sm">
                                    people detected : <span className="font-bold ml-2">{`${totalObj}`}</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
  )
}

{/* <button
                    disabled
                 // type="submit"
                    className="ml-3 inline-flex justify-center py-2 px-6 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-500 hover:bg-blue-600"
                    >
                    Search
                </button> */}

export default Poi2