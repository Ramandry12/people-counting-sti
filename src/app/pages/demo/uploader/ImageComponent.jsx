import React from 'react';

const ImageComponent = ({ base64String }) => {
  const imageSource = `data:image/jpeg;base64,${base64String}`;

  return (
    <div>
      <img src={imageSource} alt="Gambar" />
    </div>
  );
};

export default ImageComponent;
