import React, { useState } from 'react';
import axios from 'axios';

function ImageUploader() {
    const apiUrl = 'http://103.140.90.10:9091/people_counting';
    const [selectedImage, setSelectedImage] = useState(null);
    const [uploadedImage, setUploadedImage] = useState(null);
    const [b64img, setb64img] = useState(null);

    const handleImageUpload = (event) => {
        const file = event.target.files[0];
        setSelectedImage(file);

        const reader = new FileReader();
        reader.onloadend = () => {
            setUploadedImage(reader.result);
        };
        reader.readAsDataURL(file);
    };

    const handleImageSubmit = () => {
        const formData = new FormData();
        const b64img = formData.append('b64img', selectedImage);
        const body = {
            b64img: b64img
        }
        axios
            .post(apiUrl, body)
            .then((response) => {
                const imageUrl = response?.result?.b64img;
                setUploadedImage(imageUrl);
            })
            .catch((error) => {
                console.error('Error uploading image: ', error);
            });
    };

    return (
        <div>
            <input type="file" value={b64img} name='b64img' accept="image/*" onChange={handleImageUpload} />
            <button onClick={handleImageSubmit}>Upload Gambar</button>

            {uploadedImage && (
                <div>
                    <h2>Gambar Terupload</h2>
                    <img src={uploadedImage} alt="Uploaded" style={{ width: '300px' }} />
                </div>
            )}
        </div>
    );
}

export default ImageUploader;
