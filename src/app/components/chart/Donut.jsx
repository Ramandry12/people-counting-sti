import React from 'react'
import ReactApexChart from 'react-apexcharts';

export default function Donut() {
    const busList = ['BIH001', 'BIH002', 'BIH003', 'BIH004', 'BIH005', 'BIH006', 'BIH007']
    const options = {
        labels: busList,
        // colors: ['#FF4500', '#FF8C00', '#FFC700', '#00BFFF', '#9400D3'],
        responsive: [
            {
                breakpoint: 480,
                options: {
                chart: {
                    width: 300,
                },
                legend: {
                    position: 'bottom',
                },
                dataLabels: {
                    enabled: true,
                    },
                },
            },
            ],
        legend: {
        position: 'bottom',
        },
        chart: {
            toolbar: {
              show: true,
              tools: {
                download: true, // Aktifkan fitur download
                selection: false, // Nonaktifkan fitur selection
                zoom: true, // Aktifkan fitur zoom
                zoomin: true, // Aktifkan fitur zoom in
                zoomout: true, // Aktifkan fitur zoom out
                pan: true, // Aktifkan fitur pan
                reset: true, // Aktifkan fitur reset
              },
            },
          },
      };
    
      const series = [44, 55, 13, 43, 22, 32, 42];
    
      return (
        <div id="chart">
          <ReactApexChart options={options} series={series} type="donut" height="350" 
          noData={{
            text: 'No data available',
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0,
            style: {
              fontSize: '14px',
              color: '#000',
            },
          }}/>
        </div>
      );
}
