import React from 'react'
import ReactApexChart from 'react-apexcharts';

export default function Line() {
    const options = {
        chart: {
          id: 'area',
        },
        xaxis: {
          type: 'datetime',
        },
        yaxis: {
          title: {
            text: 'Data',
          },
        },
        stroke: {
            curve: 'smooth',
        },
        markers: {
            size: 0,
        }
      };
    
      const series = [
        {
          name: 'Total Passengers',
          data: [
            [new Date('2023-07-05').getTime(), 40],
            [new Date('2023-07-06').getTime(), 10],
            [new Date('2023-07-07').getTime(), 40],
            [new Date('2023-07-08').getTime(), 50],
            [new Date('2023-07-09').getTime(), 30],
            [new Date('2023-07-10').getTime(), 30],
            [new Date('2023-07-11').getTime(), 40],
            [new Date('2023-07-12').getTime(), 35],
            [new Date('2023-07-13').getTime(), 50],
            [new Date('2023-07-14').getTime(), 45],
          ],
        },
      ];
    
      return (
        <div id="chart">
          <ReactApexChart options={options} series={series} type="area" height="350" />
        </div>
      );
}
