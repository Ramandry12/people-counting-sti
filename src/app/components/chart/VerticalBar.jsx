import React from 'react'
import ReactApexChart from 'react-apexcharts';

export default function VerticalBar() {
    const options = {
        chart: {
          id: 'bar-chart',
          toolbar: {
            show: true,
            tools: {
              download: true, // Aktifkan fitur download
              selection: false, // Nonaktifkan fitur selection
              zoom: true, // Aktifkan fitur zoom
              zoomin: true, // Aktifkan fitur zoom in
              zoomout: true, // Aktifkan fitur zoom out
              pan: true, // Aktifkan fitur pan
              reset: true, // Aktifkan fitur reset
            },
          },
        },
        xaxis: {
          categories: ['BIH002', 'BIH001', 'BIH004'],
        },
      };
    
      const series = [
        {
          name: 'Passengers',
          data: [55, 44, 43],
        },
      ];
    
      return (
        <div id="chart">
          <ReactApexChart options={options} series={series} type="bar" height="350" />
        </div>
      );
}
