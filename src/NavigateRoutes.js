import { Navigate, useLocation } from 'react-router-dom';

function NavigateRoutes({ children }) {
  // const path = window.location.pathname;
  const location = useLocation();
  const path = location.pathname;
  if (path === '/') {
    return <Navigate to="/poi" />;
  }
  return children;
}

export default NavigateRoutes;
